const schema = require('./schema.json')
const fs = require('fs')

const handleObject = (def) => {
  const props = def.properties
  if (!props) return {}

  return Object.keys(props).reduce((memo, key) => {
    const value = props[key]

    key = /\||’|\?|\(|\)/.test(key) ? `'${key}'` : key

    switch (value.type) {
      case 'object':
        memo[key] = handleObject(value)
        break
      case 'array':
        if (value.items.type === 'object') {
          memo[key] = `Array<${JSON.stringify(
            handleObject(value.items)
          )}>`.replace(/\"/g, '')
        } else {
          memo[key] = `Array<${value.items.type}>`
        }
        break
      default:
        memo[key] = value.type
    }

    return memo
  }, {})
}

const result = handleObject(schema)
const fileContents = `
interface schema ${JSON.stringify(result, null, 2)
  .replace(/:\s\\?"/g, ': ')
  .replace(/:/g, '?:')
  .replace(/",?$/gm, '')}
`
fs.writeFileSync('interface.ts', fileContents, 'utf-8')
