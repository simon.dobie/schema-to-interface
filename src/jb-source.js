const schema = require("./schema.json");
const fs = require("fs");

const handleType = (type) => {
  switch (type) {
    case "string":
      return "";
    case "number":
      return 1;
    case "boolean":
      return true;
  }
};

const handleObject = (def) => {
  const props = def.properties;
  if (!props) return {};

  return Object.keys(props).reduce((memo, key) => {
    const value = props[key];

    switch (value.type) {
      case "object":
        memo[key] = handleObject(value);
        break;
      case "array":
        if (value.items.type === "object") {
          memo[key] = [handleObject(value.items)];
        } else {
          memo[key] = [handleType(value.items.type)];
        }
        break;
      default:
        memo[key] = handleType(value.type);
    }

    return memo;
  }, {});
};

const result = handleObject(schema);
const fileContents = `
${JSON.stringify(result, null, 2).replace(/:\s/g, ": ")}
`;
fs.writeFileSync("source.json", fileContents, "utf-8");
